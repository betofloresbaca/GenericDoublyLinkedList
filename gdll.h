/******************************************************************************
 * Generic Double Linked List                                                 *
 *                                                                            *
 *  Autor: Luis Alberto Flores Baca                                           *
 *  email: betofloresbaca@gmail.com                                           *
 *  Licencia: GPL                                                             *
 ******************************************************************************/

#ifndef GDLL_H
#define GDLL_H

#include <stdbool.h>
#include <stdlib.h>


/******************************************************************************
 *          Especificadores de comportamiento de las funciones                *
 ******************************************************************************/

// Tipo de dato
typedef enum{GDLL_RAW, GDLL_WRAPED} gdll_data_t;

// Posicion de referencia en una lista
typedef enum{GDLL_BEGIN, GDLL_END} gdll_ref_t;

// Tipo de movimiento en un recorrido
typedef enum{GDLL_FORWARD, GDLL_BACKWARD} gdll_move_t;


/******************************************************************************
 *                                  Estructuras                               *
 ******************************************************************************/

typedef struct gdll_Node gdll_Node;
typedef struct gdll_List gdll_List;
typedef struct gdll_Iterator gdll_Iterator;

struct gdll_Node{
  void *ptr;
  gdll_Node *next;
  gdll_Node *prev;
};

struct gdll_List{
  unsigned len;
  gdll_Node *begin;
  gdll_Node *end;
};

struct gdll_Iterator{
  gdll_Node *current; 
};


/******************************************************************************
 *                                 Utils                                      *
 ******************************************************************************/

gdll_Node *gdll_wrap(void *);
gdll_Node *gdll_conditional_wrap(void *, gdll_data_t);
 

/******************************************************************************
 *     Insercion/Obtencion de datos al inicio y al final de la lista          *
 ******************************************************************************/

bool gdll_push(gdll_List *, void *, gdll_data_t, gdll_ref_t);
bool gdll_insert(gdll_List *, void *, gdll_data_t, int(*)(void *, void *));
void *gdll_pop(gdll_List *, gdll_data_t, gdll_ref_t);


/******************************************************************************
 *                          Manejo de iteradores                              *
 ******************************************************************************/

gdll_Iterator gdll_make_iterator(gdll_List *, gdll_ref_t);
gdll_Iterator gdll_find(gdll_List *, int(*)(void *, void *), void *);
bool gdll_move_iterator(gdll_Iterator *, gdll_move_t);
void *gdll_get_current(gdll_Iterator, gdll_data_t);
bool gdll_remove_current(gdll_List *, gdll_Iterator *, gdll_move_t, void(*)(void *));


/******************************************************************************
 *                 Operaciones sobre los datos contenidos                     *
 ******************************************************************************/

bool gdll_apply_func(gdll_List *, bool(*)(void *, unsigned, unsigned, void *), void *);


/******************************************************************************
 *                           Vaciado de la lista                              *
 ******************************************************************************/

bool gdll_clear_list(gdll_List *, void(*)(void *));
bool gdll_free_list(gdll_List *, void(*)(void *));

#endif // GDLL_H
