/******************************************************************************
 * Generic Double Linked List                                                 *
 *                                                                            *
 *  Autor: Luis Alberto Flores Baca                                           *
 *  email: betofloresbaca@gmail.com                                           *
 *  Licencia: GPL                                                             *
 ******************************************************************************/

#include "gdll.h"


/******************************************************************************
 *                                 Utils                                      *
 ******************************************************************************/

gdll_Node *gdll_wrap(void *dato){
  gdll_Node *nuevo = NULL;
  nuevo = (gdll_Node *)calloc(1, sizeof(gdll_Node));
  if(nuevo){
    nuevo->ptr = dato;
  }
  return nuevo;
}

gdll_Node *gdll_conditional_wrap(void *ptr, gdll_data_t dt){
  gdll_Node *node = NULL;
  if(dt == GDLL_WRAPED){
    node = ptr;
  }else if(dt == GDLL_RAW){
    node = gdll_wrap(ptr);
    if(!node) return false;
  }
  return node;
}


/******************************************************************************
 *     Insercion/Obtencion de datos al inicio y al final de la lista          *
 ******************************************************************************/

bool gdll_push(gdll_List *list, void *ptr, gdll_data_t dt, gdll_ref_t rt){
  gdll_Node *aux = NULL;
  if(list && ptr){
    // Envolviendo el nodo si se necesita
    aux = gdll_conditional_wrap(ptr, dt);
    // Insertando
    if(list->len == 0){
      // Si es el primer nodo
      list->begin = aux;
      list->end = aux;
    }else{
      if(rt == GDLL_END){
	// Agregando al final
	list->end->next = aux;
	aux->prev = list->end;
	list->end = aux;
      }else if(rt == GDLL_BEGIN){
	// Agregando al inicio
	list->begin->prev = aux;
	aux->next = list->begin;
	list->begin = aux;
      }
    }
    (list->len)++;
    return true;
  }
  return false;
}

bool gdll_insert(gdll_List *list, void *ptr, gdll_data_t dt, int(*comp_func)(void *, void *)){
  gdll_Node *new_node = NULL;
  gdll_Node *after_new = NULL;
  gdll_Iterator iter;
  if(list && comp_func){
    // Envolviendo el nodo si se necesita
    new_node = gdll_conditional_wrap(ptr, dt);
    if(list->len == 0){
      // Si es el primer nodo
      list->begin = new_node;
      list->end = new_node;
    }else{
      //Iterando por la lista hasta encontrar el siguiente al nodo a insertar
      iter = gdll_make_iterator(list, GDLL_BEGIN);
      while((after_new = gdll_get_current(iter, GDLL_WRAPED))!=NULL){
	if(after_new->ptr != NULL && comp_func(after_new->ptr, ptr) > 0) break;
	gdll_move_iterator(&iter, GDLL_FORWARD);
      }
      if(after_new == NULL){
	//Se insertara al final
	new_node->prev = list->end;
	list->end->next = new_node;
	list->end = new_node;
      }else{
	new_node->prev = after_new->prev;
	new_node->next = after_new;
	if(list->begin == after_new){
	  //Si se inserto al inicio
	  list->begin = new_node;
	}else{
	  //Si no es al inicio
	  after_new->prev->next = new_node;
	}
	after_new->prev = new_node;
      }
    }
    (list->len)++;
    return true;
  }
  return false;

}

void *gdll_pop(gdll_List *list, gdll_data_t dt, gdll_ref_t rt){
  gdll_Node *aux = NULL;
  void *ptr = NULL;
  if(list && list->len > 0){
    // Desligando el nodo
    if(rt == GDLL_END){
      // Si es el ultimo
      aux = list->end;
      list->end = list->end->prev;
      if(list->len == 1){
	list->begin = NULL;
      }else{
	aux->prev = NULL;
	list->end->next = NULL;
      }
    }else if(rt == GDLL_BEGIN){
      // Si es el primero
      aux = list->begin;
      list->begin = list->begin->next;
      if(list->len == 1){
	list->end = NULL;
      }else{
	aux->next = NULL;
	list->begin->prev = NULL;
      }
    }
    // Preparando la salida
    if(dt == GDLL_WRAPED){
      ptr = aux; 
    }else if(dt == GDLL_RAW){
      ptr = aux->ptr;
      free(aux);
    }
    (list->len)--;
  }
  return ptr;
}


/******************************************************************************
 *                          Manejo de iteradores                              *
 ******************************************************************************/

gdll_Iterator gdll_make_iterator(gdll_List *list, gdll_ref_t posRef){
  gdll_Iterator iter;
  iter.current = NULL;
  if(list){
    if(posRef == GDLL_BEGIN){
      iter.current = list->begin;
    }else if(posRef == GDLL_END){
      iter.current = list->end;
    }
  }
  return iter;
}

gdll_Iterator gdll_find(gdll_List *list, int(*comp_func)(void *, void *), void *data){
  gdll_Iterator iter;
  void *ptr = NULL;
  iter.current = NULL;
  if(list && comp_func && data){
    iter = gdll_make_iterator(list, GDLL_BEGIN);
    while((ptr = gdll_get_current(iter, GDLL_RAW))!=NULL){
      if(comp_func(ptr, data) == 0) break;
      gdll_move_iterator(&iter, GDLL_FORWARD);
    }
  }
  return iter;
}

bool gdll_move_iterator(gdll_Iterator *iter, gdll_move_t mt){
  bool retval = false;
  if(iter && iter->current){
    if(mt == GDLL_FORWARD){
      iter->current = iter->current->next;
      retval = (iter->current != NULL);
    }else if(mt == GDLL_BACKWARD){
      iter->current = iter->current->prev;
      retval = (iter->current != NULL);
    }
  }
  return retval;
}

void *gdll_get_current(gdll_Iterator iter, gdll_data_t dt){
  void *ptr = NULL;
  if(iter.current){
    if(dt == GDLL_WRAPED){
      ptr = iter.current;
    }else if(dt == GDLL_RAW){
      ptr = iter.current->ptr;
    }
  }
  return ptr;
}

bool gdll_remove_current(gdll_List *list, gdll_Iterator *iter, gdll_move_t mt, void(*free_data)(void *)){
  gdll_Node *aux = NULL;
  if(list && iter && iter->current){
    // Tomando el elemento actual
    aux = iter->current;
    // Recorriendo it->current al nuevo elemento
    if(mt == GDLL_FORWARD){
      iter->current = iter->current->next;
    }else if(mt == GDLL_BACKWARD){
      iter->current = iter->current->prev;
    }
    // Recorriendo begin y end si es necesario
    if(aux == list->begin) list->begin = list->begin->next;
    if(aux == list->end) list->end = list->end->prev;
    // Desligando el elemento a extraer
    if(aux->prev) aux->prev->next = aux->next;
    if(aux->next) aux->next->prev = aux->prev;
    // Reduciendo el tamaño de la lista
    (list->len)--;
    // Terminando la extraccion
    if(aux->ptr && free_data){
      free_data(aux->ptr);
    }
    free(aux);
    return true;
  }
  return false;
}


/******************************************************************************
 *                 Operaciones sobre los datos contenidos                     *
 ******************************************************************************/

bool gdll_apply_func(gdll_List *list, bool(*func)(void *, unsigned, unsigned, void *), void *extra_data){
  bool retval = false;
  gdll_Iterator it;
  void *ptr = NULL;
  unsigned index = 0;
  if(list && func){
    retval = true;
    it = gdll_make_iterator(list, GDLL_BEGIN);
    while((ptr = gdll_get_current(it, GDLL_RAW))!=NULL){
      retval = retval && func(ptr, index++, list->len, extra_data);
      gdll_move_iterator(&it, GDLL_FORWARD);
    }
  }
  return retval;
}


/******************************************************************************
 *                           Vaciado de la lista                              *
 ******************************************************************************/

bool gdll_clear_list(gdll_List *list, void(*free_data)(void *)){
  void *ptr = NULL;
  if(list){
    while(list->len > 0){
      ptr = gdll_pop(list, GDLL_RAW, GDLL_BEGIN);
      if(ptr && free_data){
	free_data(ptr);
      }
    }
    return true;
  }
  return false;
}

bool gdll_free_list(gdll_List *list, void(*free_data)(void *)){
  if(gdll_clear_list(list, free_data)){
    free(list);
    return true;
  }
  return false;
}
