CC = gcc
CFLAGS = -Wall -g
LDFLAGS= -L.
LDLIBS = -lgdll
LIB_OBJECTS = gdll.o
.PHONY: all
all: libgdll.a gdll_test
libgdll.a: $(LIB_OBJECTS)
	$(AR) r $@ $^
clean:
	$(RM) *~ *.o libgdll.a gdll_test
