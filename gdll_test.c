#include <stdio.h>
#include <time.h>
#include "gdll.h"

bool print_int(void *ptr, unsigned index, unsigned len, void *extra_data){
  return printf("%d/%d :  %d\n", ++index, len, *(int*)ptr);
}

int comp(void *x, void *y){
  return *(int*)x - *(int*)y;
}

int test(){
  gdll_List *list = (gdll_List *)calloc(1, sizeof(gdll_List));
  int i;
  int *val;
  //Llenando la lista
  for(i = 0; i<10; i++){
    val = (int *)calloc(1, sizeof(int));
    *val = i;
    gdll_push(list, val, GDLL_RAW, GDLL_END);
  }
  puts("Estado inicial");
  gdll_apply_func(list, print_int, NULL);
  puts("\nSe saca el elemento");
  //Sacando un elemento
  if( (val = gdll_pop(list, GDLL_RAW, GDLL_END)) ){
    printf("%d", *val);
    free(val);
  }
  puts("\nAhora la lista es");
  gdll_apply_func(list, print_int, NULL);
  // Eliminando el 5
  *val = 5;
  gdll_Iterator iter = gdll_find(list, comp, val);
  gdll_remove_current(list, &iter, GDLL_FORWARD, free);
  puts("\nDespues de eliminar el 5");
  gdll_apply_func(list, print_int, NULL);
  gdll_free_list(list, free);
  puts("\nDespues de liberarla");
  gdll_apply_func(list, print_int, NULL);
  return 0;
}

int main(){
  gdll_List *list = (gdll_List *)calloc(1, sizeof(gdll_List));
  int i;
  int *val = NULL;
  srand(time(NULL));
  for(i=0; i<10; i++){
    val = (int *)calloc(1, sizeof(int));
    *val = rand()%10;
    gdll_insert(list, val, GDLL_RAW, comp);
    gdll_apply_func(list, print_int, NULL);
    puts("");
    getchar();
  }
  gdll_free_list(list, free);
  return 0;
}
